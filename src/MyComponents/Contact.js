import React, { useState, useEffect } from 'react'
import axios  from 'axios'
import { Typography, Select, FormControl,  MenuItem , InputLabel, TextField, Button, AppBar, Card, CardActions, CardContent, CardMedia, Grid, Toolbar, Container, Popover} from '@mui/material'
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import styles from '../style.module.css';


export const Contact = () => {

    const [data, setData] = useState([]);
    const [search, setsearch] = useState("");

    const [anchor, setAnchor] = useState(null)
    const openPopover = (e) => {
        setAnchor(e.currentTarget);
    }


    

    useEffect(() => {
        
        const fetchData = async () => {
        await axios.get('https://randomuser.me/api/?results=40')
        .then(res => {
            console.log(res);
            console.log(res.data.results);
            setData(res.data.results)
            
        }) 
        .catch(err => {
            console.log(err);
        })
        console.log("Testing-1:",data);
    }

    fetchData()
    
    // eslint-disable-next-line react-hooks/exhaustive-deps
    },[])

    return (
        <div>
            <AppBar position='relative' color='secondary'> 
            <Toolbar>
            <AccountBoxIcon className={styles.icon}/>
            <TextField id="outlined-basic" label="Search..." variant="outlined" onChange={e =>{setsearch(e.target.value)}}/>
            </Toolbar>

            
            </AppBar>


            <FormControl fullWidth style={{marginTop:"20px",width:"40%", marginLeft:"20px"}}>
            <InputLabel id="demo-simple-select-label">Location</InputLabel>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                label="Location"
                onChange={(e)=>setsearch(e.target.value)}
            >
                <MenuItem value={""}>None</MenuItem>
                <MenuItem value={"Turkey"}>Turkey</MenuItem>
                <MenuItem value={"Finland"}>Finland</MenuItem>
                <MenuItem value={"Canada"}>Canada</MenuItem>
                <MenuItem value={"Spain"}>Spain</MenuItem>
                <MenuItem value={"Switzerland"}>Switzerland</MenuItem>
                <MenuItem value={"Iran"}>Iran</MenuItem>
                <MenuItem value={"Denmark"}>Denmark</MenuItem>
                <MenuItem value={"Brazil"}>Brazil</MenuItem>
                <MenuItem value={"Germany"}>Germany</MenuItem>

            </Select>
            </FormControl>



            <main>
                <Container maxWidth='sm' className={styles.container}>
                <Typography variant='h2' align='center' color='textPrimary' gutterBottom>
                    Sample Data
                </Typography>

                <Typography variant='h6' align='center' color='textSecondary' paragraph> 
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Debitis aliquid earum sit laborum commodi. Eos quisquam unde ullam illum laboriosam!
                </Typography>

                <div className={styles.container}>
                    <Grid container spacing={2} justifyContent='center' gutterBottom>
                    <Grid item>


                        <Button variant='contained' color='secondary' onClick={openPopover}>
                        Pop Over
                        </Button>

                        <Popover 
                        open={Boolean(anchor)}
                        anchorEl={anchor}
                        anchorOrigin={{
                            vertical:'top',
                            horizontal:'right'
                        }}
                        transformOrigin={{
                            vertical:'bottom',
                            horizontal:'left'
                        }}
                        onClose={()=>setAnchor(null)}
                        >
                            <Typography variant='h6' style={{padding:"10px"}}>
                                This is a Pop Over
                            </Typography>
                        </Popover>


                    </Grid>
                    <Grid item> 
                        <Button variant='outlined' color='primary'>
                        Next Action
                        </Button>
                    </Grid>
                    </Grid>
                </div>

                </Container>

                <container maxWidth='md'>
                <Grid container spacing={4}> 
                {data.filter((val) => {
                    if(search===""){
                        return val
                    }
                    else if(val.location.country.toLowerCase().includes(search.toLowerCase()))
                        return val
                    else
                        return false
                    
                }).map((card) =>(
                    <Grid item key={card.phone} xs={12} sm={6} md={4}> 
                    <Card className={styles.card}>
                    <CardMedia
                    image={card.picture.large}
                    className={styles.cardMedia}
                    />

                    <CardContent className={styles.cardContent}>
                        <Typography gutterBottom variant='h5'>
                        {card.name.first}
                        </Typography>

                        <Typography>
                        I'm From {card.location.country}
                        </Typography>
                    </CardContent>

                    <CardActions>
                        <Button size='small' color='primary'>View</Button>
                        <Button size='small' color='primary'>Edit</Button>
                    </CardActions>
                    </Card>
                </Grid>
                ))}
                    
                </Grid>

                </container>
            </main>
        </div>
    )
}
